package com.app.fusionandroidjava;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;


public class MainActivity extends AppCompatActivity {
    static {
        System.loadLibrary("opencv_java3");
    }

    TextView FpsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Bitmap bitmap = drawJPG("gray4.jpg");
        Bitmap bitmap1= drawJPG("rgb4.jpg");
        ImageView imageView=(ImageView)findViewById(R.id.imageView);
        ImageView imageView1= (ImageView)findViewById(R.id.imageView2);
        imageView1.setImageBitmap(bitmap1);
        FusionAlgorithmOptimLogs algo= new FusionAlgorithmOptimLogs();
        Mat gray= readImageFromBitmap(bitmap);
        Mat color= readImageFromBitmap(bitmap1);
        long start= System.currentTimeMillis();
        algo.entryPoint(bitmap1,bitmap);
        algo.entryPoint(bitmap1,bitmap);
        algo.entryPoint(bitmap1,bitmap);
//        algo.entryPoint(bitmap1,bitmap);
//        algo.entryPoint(bitmap1,bitmap);
//        algo.entryPoint(bitmap1,bitmap);
//        algo.entryPoint(bitmap1,bitmap);
        long end= System.currentTimeMillis();

        Map<String,Long> logs= Logger.getInstance().getLogs();
        Set<String> keys= logs.keySet();

        for ( Map.Entry<String, Long> entry : logs.entrySet()) {
            String key = entry.getKey();
            Long value = entry.getValue();

            Log.e("Time Logs : ", key+" :: "+String.valueOf(value));
            // do something with key and/or tab
        }


//        Toast.makeText(this, "FPS : "+String.valueOf((end-start)/(4*1000)),Toast.LENGTH_LONG).show();
        ((TextView)findViewById(R.id.hel)).setText("Execution time millis : "+String.valueOf((end-start)));
        imageView.setImageBitmap(algo.entryPoint(bitmap1,bitmap));

//        Mat gray= new Mat();


    }

    private Bitmap drawJPG(String name) {
        Context context = null;
        InputStream is = null;
        context = getApplicationContext();
        try {
            is = context.getAssets().open(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return bitmap;
    }
    private Mat readImageFromBitmap(Bitmap bmp) {
        Mat mat = new Mat();
        Bitmap bmp32 = bmp.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmp32, mat);
        return mat;
    }


    private Bitmap showImg(Mat img) {
        Bitmap bm = Bitmap.createBitmap(img.cols(), img.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img, bm);
        return bm;
//        ImageView imageView = (ImageView) findViewById(R.id.img_view);
//        imageView.setImageBitmap(bm);
    }
}
