package com.app.fusionandroidjava;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class FusionAlgorithmOptimLogs {

    Long start;
    Long end;
    private Mat readImageFromBitmap(Bitmap bmp) {
        Mat mat = new Mat();
        Bitmap bmp32 = bmp.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmp32, mat);
        return mat;
    }
    private Bitmap showImg(Mat img) {
        Bitmap bm = Bitmap.createBitmap(img.cols(), img.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img, bm);
        return bm;
//        ImageView imageView = (ImageView) findViewById(R.id.img_view);
//        imageView.setImageBitmap(bm);
    }
    public Mat entryPoint2(Mat inputRGBImage, Mat grayImage){

        int numChannels=inputRGBImage.channels();
        Mat rgbImage=new Mat();
        if(numChannels==1){
            Imgproc.cvtColor(inputRGBImage, rgbImage, Imgproc.COLOR_GRAY2BGRA);
        }
        else if(numChannels==3){
            Imgproc.cvtColor(inputRGBImage, rgbImage, Imgproc.COLOR_BGR2RGBA);
        }
        Mat grayImageMat= new Mat();
        Imgproc.cvtColor(grayImage,grayImageMat, Imgproc.COLOR_BGR2GRAY);
//        Imgproc.GaussianBlur(grayImageMat.clone(), grayImageMat, new Size(7,7),0,0);
        Imgproc.GaussianBlur(grayImageMat, grayImageMat, new Size(7,7),0,0);
        Mat equializedMat= new Mat();
//        Imgproc.equalizeHist(grayImageMat.clone(),equializedMat);
        Imgproc.equalizeHist(grayImageMat,equializedMat);

        Mat mappedGrayMat= new Mat();
//        Imgproc.applyColorMap(equializedMat.clone(), mappedGrayMat, Imgproc.COLORMAP_JET);

        start= System.currentTimeMillis();
        Imgproc.applyColorMap(equializedMat, mappedGrayMat, Imgproc.COLORMAP_JET);
        end= System.currentTimeMillis();
        Logger.getInstance().increment("ApplyingColorMapJet", end-start);


        start= System.currentTimeMillis();

        Mat  mask= createMaskMat(mappedGrayMat);
        end= System.currentTimeMillis();

        Logger.getInstance().increment("createMaskMat",end-start);
//  Create BlackProcessing mask mat

//        Mat maskBlack= createBlackProcessingMaskMat(mappedGrayMat.clone(), mask,
//                equializedMat.clone());
        start= System.currentTimeMillis();
        Mat maskBlack= createBlackProcessingMaskMat(mappedGrayMat, mask,
                equializedMat);
        end= System.currentTimeMillis();

        Logger.getInstance().increment("createBlackProcessingMaskMat",end-start);


        Mat eqMask= new Mat(equializedMat.rows(),equializedMat.cols(),equializedMat.type());
        Core.multiply(equializedMat,mask,eqMask);

        Mat rgbCombined= new Mat();
//        Imgproc.cvtColor(equializedMat.clone(),rgbCombined,Imgproc.COLOR_GRAY2BGRA);
        Imgproc.cvtColor(equializedMat,rgbCombined,Imgproc.COLOR_GRAY2BGRA);
        List<Mat> maskBlackBGRColor= new ArrayList<>(3);

        Core.split(maskBlack,maskBlackBGRColor);

        start= System.currentTimeMillis();
        maskBlack= createResizedMask(maskBlackBGRColor.get(0), inputRGBImage);
        end= System.currentTimeMillis();

        Logger.getInstance().increment("createResizedMask",end-start);
//        Mat colorImage= inputRGBImage.clone();
        Mat colorImage= inputRGBImage;
        List<Mat> channelColorImage= new ArrayList<Mat>(3);
        start= System.currentTimeMillis();

//        Core.split(colorImage,channelColorImage);
//        Mat g= channelColorImage.get(0);
//        Mat b= channelColorImage.get(1);
//        Mat r= channelColorImage.get(2);
//
////        left side of addition
//        Mat mg= new Mat();
//        Mat mb= new Mat();
//        Mat mr= new Mat();
//        Core.multiply(maskBlack,new Scalar(123), mg);
//        Core.multiply(maskBlack, new Scalar(233),mb);
//        Core.multiply(maskBlack, new Scalar(23), mr);
//
//
////        Operand which would work on color channels
//        Mat operandR= new Mat();
//
//        Core.subtract(maskBlack,new Scalar(255),operandR);
//        Core.multiply(operandR,new Scalar(-1),operandR);
//        for(int x=0; x<operandR.rows();x++){
//            for(int y=0; y<operandR.cols(); y++){
//                Log.e("All pixels", String.valueOf(operandR.get(x,y)[0]));
//            }
//        }
//
////        Right side of addition rgb channels
//        Mat gr= new Mat();
//        Mat br= new Mat();
//        Mat rr= new Mat();
//
//        Core.multiply(operandR,g,gr);
//        Core.multiply(operandR,b,br);
//        Core.multiply(operandR,r,rr);
//
////        final color channels
//        Core.add(mg,gr,g);
//        Core.add(mb,br,b);
//        Core.add(mr,rr,r);
//        List<Mat> finalChannelList= new ArrayList<Mat>(3);
//        finalChannelList.add(g);
//        finalChannelList.add(b);
//        finalChannelList.add(r);
//        Mat finalImage= new Mat();
//        Core.merge(finalChannelList,finalImage);
//
//
//        colorImage=finalImage;
//

//        for(int y=0; y<colorImage.rows();y++){
//            for(int x=0; x<colorImage.cols(); x++){
//                double[] blackMaskColor= maskBlack.get(y,x);
//                if(blackMaskColor[0]==255){
//                    double[] color= colorImage.get(y,x);
//                    color[0]=123;
//                    color[1]=233;
//                    color[2]=23;
//                    colorImage.put(y,x,color);
//                }
//            }
//        }
        end= System.currentTimeMillis();
        Logger.getInstance().increment("HighlightROI",end-start );
        start= System.currentTimeMillis();
        colorImage=boundROI(colorImage,maskBlack);
        end= System.currentTimeMillis();
        Logger.getInstance().increment("boundROI : ", end-start);

//        create resized mask

//        Mat eqMask=
//        Mat eqMask= Core.multiply();



        return colorImage;

    }

    public Bitmap entryPoint(Bitmap inputRGBBitmap, Bitmap grayImageBitmap){

        Mat inputRGBImage= readImageFromBitmap(inputRGBBitmap);
        Mat grayImage= readImageFromBitmap(grayImageBitmap);
        Mat colorImage=entryPoint2(inputRGBImage,grayImage);

        return showImg(colorImage);
    }

    public Mat boundROI(Mat image, Mat mask){
        List<MatOfPoint> contours=createContours(mask,60,false);
        for(MatOfPoint contour: contours){
            MatOfPoint2f approxCurve = new MatOfPoint2f();
            MatOfPoint2f contour2f = new MatOfPoint2f( contour.toArray() );
            double approxDistance = Imgproc.arcLength(contour2f, true)*0.02;
            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);
            MatOfPoint points = new MatOfPoint( approxCurve.toArray() );
            Rect rect = Imgproc.boundingRect(points);
            image= highlightRoi(image,mask,rect.x,rect.y,rect.height,rect.width);
            Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0, 255), 3);
        }

        return image;
    }

    public Mat highlightRoi(Mat colorImage, Mat mask, int x, int y, int height, int width){

        for(int i= x; i< x+width;i++){
            for(int j=y; j<y+height; j++){
                double[] colorMask= mask.get(j,i);

                if(colorMask[0]==255){
                    double[] color= colorImage.get(j,i);
                    color[0]=123;
                    color[1]=233;
                    color[2]=23;
                    colorImage.put(j,i,color);
                }
            }
        }
        return colorImage;
    }

    public Mat createResizedMask(Mat sourceMat, Mat destinationMat){
        Mat resize= transformation(sourceMat,destinationMat);
        return resize;
    }

    public Mat transformation(Mat img, Mat rgb){

        MatOfPoint2f pts1= new MatOfPoint2f(new Point(147,188),
                new Point(882,225),new Point(151,1239),
                new Point(837,1240));
        MatOfPoint2f pts2= new MatOfPoint2f(new Point(165,178),
                new Point(363,185),new Point(171,459),
                new Point(356,460));

        Mat M= Imgproc.getPerspectiveTransform(pts2,pts1);
        Mat resized= new Mat();

        Imgproc.warpPerspective(img,resized,M, new Size(rgb.size().width,
                rgb.size().height));
        return resized;

    }

    public List<MatOfPoint> createContours( Mat contourInput, int area, boolean small){
        Mat image= new Mat();
        contourInput.copyTo(image);
        List<MatOfPoint> finalContours= new ArrayList<MatOfPoint>();
        final Mat hierarchy = new Mat();
        List<MatOfPoint> contours= new ArrayList<MatOfPoint>();

        Imgproc.findContours(image,contours,hierarchy,Imgproc.RETR_TREE,Imgproc.CHAIN_APPROX_SIMPLE);
        for(MatOfPoint contour : contours){
            double contourArea= Imgproc.contourArea(contour);
            if(contourArea> area && !small){
                finalContours.add(contour);
            }
            else if (contourArea < area && small){
                finalContours.add(contour);
            }
        }
        return finalContours;

    }

    public Mat createMaskMat(Mat thermal){
        List<Mat> components= new ArrayList<>(3);

        Core.split(thermal, components);
        Mat redImage= components.get(2);
        Mat contourInputMat= new Mat();
        Imgproc.GaussianBlur(redImage,contourInputMat,new Size(7,7), 0);
        Core.inRange(contourInputMat,new Scalar(200),new Scalar(255),contourInputMat);
//        for(int y=0; y<contourInputMat.rows(); y++){
//            for(int x=0; x< contourInputMat.cols(); x++){
//                double color=0;
//                double[] pixel= contourInputMat.get(y,x);
//                if(pixel[0]<=200){
//                    color=0;
//                }
//                else {
//                    color= 255;
//                }
//                contourInputMat.put(y,x,color);
//            }
//        }


        Imgproc.morphologyEx(contourInputMat,contourInputMat,
                Imgproc.MORPH_CLOSE, Mat.ones(new Size(3,3),
                        CvType.CV_32F), new Point(-1,-1),2);
        //create contour
        List<MatOfPoint> contours= createContours(contourInputMat, 20000, false);
        Mat mask=new Mat(contourInputMat.rows(),contourInputMat.cols(),redImage.type());

        mask.setTo(new Scalar(0));
        Imgproc.fillPoly(mask,contours,new Scalar(255,255,255));
        List<Mat> maskBGRColor= new ArrayList<>(3);
        Core.split(mask,maskBGRColor);
        Imgproc.morphologyEx(maskBGRColor.get(0), mask,
                Imgproc.MORPH_CLOSE, Mat.ones(13,13, CvType.CV_32F),
                new Point(-1,-1),5);

        return mask;


    }
    MatOfPoint hull2Points(MatOfInt hull, MatOfPoint contour) {
        List<Integer> indexes = hull.toList();
        List<Point> points = new ArrayList<>();
        MatOfPoint point= new MatOfPoint();
        for(Integer index:indexes) {
            points.add(contour.toList().get(index));
        }
        point.fromList(points);
        return point;
    }
    public Mat createBlackProcessingMaskMat(Mat inputBlack, Mat mask, Mat equalized){
        Mat seedsImage= equalized.clone();

        Core.inRange(seedsImage,new Scalar(0),new Scalar(4),seedsImage);

//        for(int y=0; y< seedsImage.rows(); y++){
//            for(int x=0; x<seedsImage.cols(); x++){
//                int icolor=0;
//                double[] pixel= seedsImage.get(y,x);
//
//                if(pixel[0] <=4){
//                    icolor=255;
//                }else{
//                    icolor=0;
//                }
//
//                double color= Double.valueOf(icolor);
//
//
//                seedsImage.put(y,x,color);
//
//            }
//        }

        Mat multiplyResult= new Mat(seedsImage.rows(),seedsImage.cols(),seedsImage.type());
        Core.bitwise_and(seedsImage,mask,multiplyResult);

        List<MatOfPoint> contours= createContours(multiplyResult,60,false);
        Mat mask1= mask.clone();
        mask1.setTo(new Scalar(0,0,0));
        List<MatOfInt> hullList= new ArrayList<>(contours.size());
        List<MatOfPoint> hullContours= new ArrayList<MatOfPoint>();
        for(MatOfPoint contour: contours){
            MatOfInt hull= new MatOfInt();
            Imgproc.convexHull(contour,hull,false);
            MatOfPoint matHull=hull2Points(hull,contour);
            hullContours.add(matHull);

        }
        Imgproc.fillPoly(mask1,hullContours,new Scalar(255,255,255));


        return mask1;

    }

}
