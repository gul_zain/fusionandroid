package com.app.fusionandroidjava;

import java.util.HashMap;
import java.util.Map;

public class Logger {

    static Logger object;
    Map<String, Long> logs;


    public static Logger getInstance(){
        if(object==null){
            object= new Logger();
        }
        return object;
    }

    public Logger(){
        logs= new HashMap<String, Long>();
    }

    public Map<String, Long> getLogs(){
        return logs;
    }

    public void increment(String key, Long time){
        if(logs.containsKey(key)){
            logs.put(key,logs.get(key)+time);
        }
        else {
            logs.put(key,time);
        }
    }


}
