package com.app.fusionandroidjava;

import android.graphics.Bitmap;
import android.util.Log;



import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Core;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;

import org.opencv.imgproc.Imgproc;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class FusionAlgorithm {
    private Mat readImageFromBitmap(Bitmap bmp) {
        Mat mat = new Mat();
        Bitmap bmp32 = bmp.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmp32, mat);
        return mat;
    }
    private Bitmap showImg(Mat img) {
        Bitmap bm = Bitmap.createBitmap(img.cols(), img.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img, bm);
        return bm;
//        ImageView imageView = (ImageView) findViewById(R.id.img_view);
//        imageView.setImageBitmap(bm);
    }
    public Mat entryPoint2(Mat inputRGBImage, Mat grayImage){

        int numChannels=inputRGBImage.channels();
        Mat rgbImage=new Mat();
        if(numChannels==1){
            Imgproc.cvtColor(inputRGBImage, rgbImage, Imgproc.COLOR_GRAY2BGRA);
        }
        else if(numChannels==3){
            Imgproc.cvtColor(inputRGBImage, rgbImage, Imgproc.COLOR_BGR2RGBA);
        }
        Mat grayImageMat= new Mat();
        Imgproc.cvtColor(grayImage,grayImageMat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(grayImageMat.clone(), grayImageMat, new Size(7,7),0,0);
        Mat equializedMat= new Mat();
        Imgproc.equalizeHist(grayImageMat.clone(),equializedMat);

        Mat mappedGrayMat= new Mat();
        Imgproc.applyColorMap(equializedMat.clone(), mappedGrayMat, Imgproc.COLORMAP_JET);


        Mat  mask= createMaskMat(mappedGrayMat);
//        Create BlackProcessing mask mat

        Mat maskBlack= createBlackProcessingMaskMat(mappedGrayMat.clone(), mask,
                equializedMat.clone());

        Mat eqMask= new Mat(equializedMat.rows(),equializedMat.cols(),equializedMat.type());
        Core.multiply(equializedMat,mask,eqMask);
        Mat rgbCombined= new Mat();
        Imgproc.cvtColor(equializedMat.clone(),rgbCombined,Imgproc.COLOR_GRAY2BGRA);
        List<Mat> maskBlackBGRColor= new ArrayList<>(3);

        Core.split(maskBlack,maskBlackBGRColor);
        maskBlack= createResizedMask(maskBlackBGRColor.get(0), inputRGBImage);

        Mat colorImage= inputRGBImage.clone();
        for(int y=0; y<colorImage.rows();y++){
            for(int x=0; x<colorImage.cols(); x++){
                double[] blackMaskColor= maskBlack.get(y,x);
                if(blackMaskColor[0]==255){
                    double[] color= colorImage.get(y,x);
                    color[0]=123;
                    color[1]=233;
                    color[2]=23;
                    colorImage.put(y,x,color);
                }
            }
        }

        colorImage=boundROI(colorImage,maskBlack);

//        create resized mask

//        Mat eqMask=
//        Mat eqMask= Core.multiply();



        return colorImage;

    }

    public Bitmap entryPoint(Bitmap inputRGBBitmap, Bitmap grayImageBitmap){

        Mat inputRGBImage= readImageFromBitmap(inputRGBBitmap);
        Mat grayImage= readImageFromBitmap(grayImageBitmap);
        int numChannels=inputRGBImage.channels();
        Mat rgbImage=new Mat();
        if(numChannels==1){
            Imgproc.cvtColor(inputRGBImage, rgbImage, Imgproc.COLOR_GRAY2BGRA);
        }
        else if(numChannels==3){
            Imgproc.cvtColor(inputRGBImage, rgbImage, Imgproc.COLOR_BGR2RGBA);
        }
        Mat grayImageMat= new Mat();
        Imgproc.cvtColor(grayImage,grayImageMat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(grayImageMat.clone(), grayImageMat, new Size(7,7),0,0);
        Mat equializedMat= new Mat();
        Imgproc.equalizeHist(grayImageMat.clone(),equializedMat);

        Mat mappedGrayMat= new Mat();
        Imgproc.applyColorMap(equializedMat.clone(), mappedGrayMat, Imgproc.COLORMAP_JET);


        Mat  mask= createMaskMat(mappedGrayMat);
//        Create BlackProcessing mask mat

        Mat maskBlack= createBlackProcessingMaskMat(mappedGrayMat.clone(), mask,
                equializedMat.clone());

        Mat eqMask= new Mat(equializedMat.rows(),equializedMat.cols(),equializedMat.type());
        Core.multiply(equializedMat,mask,eqMask);
        Mat rgbCombined= new Mat();
        Imgproc.cvtColor(equializedMat.clone(),rgbCombined,Imgproc.COLOR_GRAY2BGRA);
        List<Mat> maskBlackBGRColor= new ArrayList<>(3);

        Core.split(maskBlack,maskBlackBGRColor);
        maskBlack= createResizedMask(maskBlackBGRColor.get(0), inputRGBImage);

        Mat colorImage= inputRGBImage.clone();
        for(int y=0; y<colorImage.rows();y++){
            for(int x=0; x<colorImage.cols(); x++){
                double[] blackMaskColor= maskBlack.get(y,x);
                if(blackMaskColor[0]==255){
                    double[] color= colorImage.get(y,x);
                    color[0]=123;
                    color[1]=233;
                    color[2]=23;
                    colorImage.put(y,x,color);
                }
            }
        }

    colorImage=boundROI(colorImage,maskBlack);

//        create resized mask

//        Mat eqMask=
//        Mat eqMask= Core.multiply();



        return showImg(colorImage);
    }

    public Mat boundROI(Mat image, Mat mask){
        List<MatOfPoint> contours=createContours(mask,60,false);
        for(MatOfPoint contour: contours){
            MatOfPoint2f approxCurve = new MatOfPoint2f();
            MatOfPoint2f contour2f = new MatOfPoint2f( contour.toArray() );
            double approxDistance = Imgproc.arcLength(contour2f, true)*0.02;
            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);
            MatOfPoint points = new MatOfPoint( approxCurve.toArray() );
            Rect rect = Imgproc.boundingRect(points);
            Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0, 255), 3);
        }
        return image;
    }

    public Mat createResizedMask(Mat sourceMat, Mat destinationMat){
        Mat resize= transformation(sourceMat,destinationMat);
        return resize;
    }

    public Mat transformation(Mat img, Mat rgb){

        MatOfPoint2f pts1= new MatOfPoint2f(new Point(147,188),
                new Point(882,225),new Point(151,1239),
                new Point(837,1240));
        MatOfPoint2f pts2= new MatOfPoint2f(new Point(165,178),
                new Point(363,185),new Point(171,459),
                new Point(356,460));

        Mat M= Imgproc.getPerspectiveTransform(pts2,pts1);
        Mat resized= new Mat();

        Imgproc.warpPerspective(img,resized,M, new Size(rgb.size().width,
                rgb.size().height));
        return resized;

    }

    public List<MatOfPoint> createContours( Mat contourInput, int area, boolean small){
        Mat image= new Mat();
        contourInput.copyTo(image);
        List<MatOfPoint> finalContours= new ArrayList<MatOfPoint>();
        final Mat hierarchy = new Mat();
        List<MatOfPoint> contours= new ArrayList<MatOfPoint>();

        Imgproc.findContours(image,contours,hierarchy,Imgproc.RETR_TREE,Imgproc.CHAIN_APPROX_SIMPLE);
        for(MatOfPoint contour : contours){
            double contourArea= Imgproc.contourArea(contour);
            if(contourArea> area && !small){
                finalContours.add(contour);
            }
            else if (contourArea < area && small){
                finalContours.add(contour);
            }
        }
        return finalContours;

    }

    public Mat createMaskMat(Mat thermal){
        List<Mat> components= new ArrayList<>(3);

        Core.split(thermal, components);
        Mat redImage= components.get(2);
        Mat contourInputMat= new Mat();
        Imgproc.GaussianBlur(redImage,contourInputMat,new Size(7,7), 0);

        for(int y=0; y<contourInputMat.rows(); y++){
            for(int x=0; x< contourInputMat.cols(); x++){
                double color=0;
                double[] pixel= contourInputMat.get(y,x);
                if(pixel[0]<=200){
                    color=0;
                }
                else {
                    color= 255;
                }
                contourInputMat.put(y,x,color);
            }
        }


        Imgproc.morphologyEx(contourInputMat,contourInputMat,
                Imgproc.MORPH_CLOSE, Mat.ones(new Size(3,3),
                        CvType.CV_32F), new Point(-1,-1),2);
        //create contour
        List<MatOfPoint> contours= createContours(contourInputMat, 20000, false);
        Mat mask=new Mat(contourInputMat.rows(),contourInputMat.cols(),redImage.type());

        mask.setTo(new Scalar(0));
        Imgproc.fillPoly(mask,contours,new Scalar(255,255,255));
        List<Mat> maskBGRColor= new ArrayList<>(3);
        Core.split(mask,maskBGRColor);
        Imgproc.morphologyEx(maskBGRColor.get(0), mask,
                Imgproc.MORPH_CLOSE, Mat.ones(13,13, CvType.CV_32F),
                new Point(-1,-1),5);

        return mask;


    }
    MatOfPoint hull2Points(MatOfInt hull, MatOfPoint contour) {
        List<Integer> indexes = hull.toList();
        List<Point> points = new ArrayList<>();
        MatOfPoint point= new MatOfPoint();
        for(Integer index:indexes) {
            points.add(contour.toList().get(index));
        }
        point.fromList(points);
        return point;
    }
    public Mat createBlackProcessingMaskMat(Mat inputBlack, Mat mask, Mat equalized){
        Mat seedsImage= equalized.clone();
        for(int y=0; y< seedsImage.rows(); y++){
            for(int x=0; x<seedsImage.cols(); x++){
                int icolor=0;
                double[] pixel= seedsImage.get(y,x);
                if(pixel[0] <=4){
                    icolor=255;
                }else{
                    icolor=0;
                }

                double color= Double.valueOf(icolor);


                seedsImage.put(y,x,color);

            }
        }

        Mat multiplyResult= new Mat(seedsImage.rows(),seedsImage.cols(),seedsImage.type());
        Core.bitwise_and(seedsImage,mask,multiplyResult);

        List<MatOfPoint> contours= createContours(multiplyResult,60,false);
        Mat mask1= mask.clone();
        mask1.setTo(new Scalar(0,0,0));
        List<MatOfInt> hullList= new ArrayList<>(contours.size());
        List<MatOfPoint> hullContours= new ArrayList<MatOfPoint>();
        for(MatOfPoint contour: contours){
            MatOfInt hull= new MatOfInt();
            Imgproc.convexHull(contour,hull,false);
            MatOfPoint matHull=hull2Points(hull,contour);
            hullContours.add(matHull);

        }
        Imgproc.fillPoly(mask1,hullContours,new Scalar(255,255,255));


        return mask1;

    }

}
